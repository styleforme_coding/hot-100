package Hot_100;

import java.util.Scanner;
import java.util.Stack;

//每日温度
public class Hot_100_739 {
    /**
     * 主函数
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化数组
        int n = sc.nextInt();
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        arr = dailyTemperatures(arr);
        for (int i = 0; i < arr.length - 1; i++) {
            System.out.print(arr[i] + ",");
        }
        System.out.println(arr[arr.length - 1]);
    }

    /**
     * 求气温列表函数
     *
     * @param temperatures
     * @return
     */
    public static int[] dailyTemperatures(int[] temperatures) {
        //存储答案列表
        int ans[] = new int[temperatures.length];
        //单调栈
        Stack<Integer> stack = new Stack<>();
        //从尾部开始遍历
        for (int i = temperatures.length - 1; i >= 0; i--) {
            //如果栈内元素比数组元素小，则弹出
            while (!stack.isEmpty() && temperatures[stack.peek()] <= temperatures[i]) {
                stack.pop();
            }
            //根据栈是否为空构建气温列表
            ans[i] = stack.isEmpty() ? 0 : stack.peek() - i;
            //进栈
            stack.push(i);
        }
        return ans;
    }
}
//时间复杂度: O(n)
//空间复杂度: O(n)