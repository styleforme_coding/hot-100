package Hot_100;

import java.util.Scanner;
import java.util.Stack;

//最长有效括号
public class Hot_100_32 {
    /**
     * 主函数
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        //调用处理函数并输出结果
        System.out.println(longestValidParentheses(str));
    }

    /**
     * 求最长有效括号函数
     *
     * @param s
     * @return
     */
    public static int longestValidParentheses(String s) {
        int n = s.length();
        Stack<Integer> stack = new Stack<>();
        int dp[] = new int[n + 1];
        for (int i = 0; i < n; i++) {
            // 遇到左括号，记录索引
            if (s.charAt(i) == '(') {
                stack.push(i);
                // 左括号不可能是合法括号子串的结尾
                dp[i + 1] = 0;
                // 遇到右括号
            } else if (!stack.isEmpty()) {
                // 配对的左括号对应索引
                int leftindex = stack.pop();
                // 以这个右括号结尾的最长子串长度
                int len = 1 + i - leftindex + dp[leftindex];
                dp[i + 1] = len;
            } else {
                // 没有配对的左括号
                dp[i + 1] = 0;
            }
        }
        int res = 0;
        // 计算最长子串的长度
        for (int i = 0; i < dp.length; i++) {
            res = Math.max(dp[i], res);
        }
        return res;
    }
}
//时间复杂度: O(n)
//空间复杂度: O(n)