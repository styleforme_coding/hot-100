package Hot_100;

import java.util.Scanner;

//反转链表
public class Hot_100_206 {
    public static class ListNode {
        int val;
        ListNode next;

        public ListNode(int x) {
            val = x;
            next = null;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ListNode head = new ListNode(-1);
        System.out.print("请输入链表节点个数: ");
        int m = sc.nextInt();
        System.out.print("请输入链表节点值: ");
        head = Create(m);
        System.out.print("结果: ");
        Print(head);
        System.out.println();
        System.out.println("反转链表: ");
        head = reverseList(head);
        System.out.print("结果: ");
        Print(head);
    }

    /**
     * 创建链表函数
     *
     * @param n
     * @return
     */
    public static ListNode Create(int n) {
        Scanner sc = new Scanner(System.in);
        //定义投节点
        ListNode head = new ListNode(-1);
        ListNode p = head, q = head;
        //给头节点赋值
        p.val = sc.nextInt();
        n--;
        //依次赋值，形成链
        while (n > 0) {
            q = p;
            p = new ListNode(-1);
            p.val = sc.nextInt();
            q.next = p;
            n--;
        }
        return head;
    }

    /**
     * 反转链表函数
     *
     * @param head
     * @return
     */
    public static ListNode reverseList(ListNode head) {
        ListNode p = head, q = head;
        ListNode r = null;
        while (p != null) {
            q = p.next;
            p.next = r;
            r = p;
            p = q;
        }
        return r;
    }

    /**
     * 输出链表元素函数
     *
     * @param head
     */
    public static void Print(ListNode head) {
        for (ListNode p = head; p != null; p = p.next) {
            System.out.print(p.val + " ");
        }
    }
}
//时间复杂度: O(n)
//空间复杂度: O(1)