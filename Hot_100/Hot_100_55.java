package Hot_100;

import java.util.Scanner;

//跳跃游戏
public class Hot_100_55 {
    /**
     * 主函数
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化数组
        int n = sc.nextInt();
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        //调用处理函数并输出结果
        System.out.println(canJump(arr));
    }

    /**
     * 求是否能跳到终点
     *
     * @param nums
     * @return
     */
    public static boolean canJump(int[] nums) {
        int res = 0;
        for (int i = 0; i < nums.length - 1; i++) {
            //每次跳最大距离
            res = Math.max(res, i + nums[i]);
            //碰到0
            if (res <= i) {
                return false;
            }
        }
        return res >= nums.length - 1;
    }
}
//时间复杂度: O(n)
//空间复杂度: O(1)