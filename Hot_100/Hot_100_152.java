package Hot_100;

import java.util.Scanner;

//最大乘积子数组
public class Hot_100_152 {
    /**
     * 主函数
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化数组
        int n = sc.nextInt();
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        //调用处理函数并输出结果
        System.out.println(maxProduct(arr));
    }

    /**
     * 乘积数组
     *
     * @param nums
     * @return
     */
    public static int maxProduct(int[] nums) {
        int ans = Integer.MIN_VALUE, max = 1, min = 1;
        for (int i = 0; i < nums.length; i++) {
            //处理当前数为负数的情况，最大值变最小值，最小值变最大值
            if (nums[i] < 0) {
                int t = max;
                max = min;
                min = t;
            }
            //最大值
            max = Math.max(max * nums[i], nums[i]);
            //最小值
            min = Math.min(min * nums[i], nums[i]);
            //答案
            ans = Math.max(ans, max);
        }
        return ans;
    }
}
//时间复杂度: O(n)
//空间复杂度: O(1)