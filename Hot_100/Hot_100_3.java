package Hot_100;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

//无重复字符的最长子串
public class Hot_100_3 {
    /**
     * 主函数
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化字符串
        String str = sc.nextLine();
        //调用处理函数并输出结果
        System.out.println(lengthOfLongestSubstring(str));
    }

    /**
     * 求最长无重复子串
     *
     * @param s
     * @return
     */
    public static int lengthOfLongestSubstring(String s) {
        Map<Character, Integer> map = new HashMap<>();
        int end = 0, start = 0, ans = 0;
        //end向后遍历
        for (; end < s.length(); end++) {
            char ch = s.charAt(end);
            //如果重复
            if (map.containsKey(ch)) {
                //更新start
                start = Math.max(map.get(ch), start);
            }
            //更新ans
            ans = Math.max(ans, end - start + 1);
            //不重复则加入
            map.put(s.charAt(end), end + 1);
        }
        return ans;
    }
}
//时间复杂度: O(n)
//空间复杂度: O(n)