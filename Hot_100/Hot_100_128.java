package Hot_100;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

//最长连续序列
public class Hot_100_128 {
    /**
     * 主函数
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        //初始化数组
        int n=sc.nextInt();
        int arr[]=new int[n];
        for(int i=0;i<n;i++){
            arr[i]=sc.nextInt();
        }
        //调用处理还是并输出结果
        System.out.println(longestConsecutive(arr));
    }

    /**
     * 求最长连续序列
     * @param nums
     * @return
     */
    public static int longestConsecutive(int[] nums) {
        // 建立一个存储所有数的哈希表，同时起到去重功能
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            set.add(num);
        }

        int ans = 0;
        // 遍历所有数字，已经经过去重
        for (int num : set) {
            int cur = num;
            // 只有当num-1不存在时，才开始向后遍历num+1，num+2，num+3......
            if (!set.contains(cur - 1)) {
                while (set.contains(cur + 1)) {
                    cur++;
                }
            }
            // [num, cur]之间是连续的，数字有cur - num + 1个
            ans = Math.max(ans, cur - num + 1);
        }
        return ans;
    }
}
//时间复杂度: O(n)
//空间复杂度: O(n)