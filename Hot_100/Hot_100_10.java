package Hot_100;

import java.util.Scanner;

//正则表达式匹配
public class Hot_100_10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化字符串
        String str1 = sc.nextLine();
        String str2 = sc.nextLine();
        //调用处理函数并输出结果
        System.out.println(isMatch(str1, str2));
    }

    /**
     * 判断正则表达式
     *
     * @param s
     * @param p
     * @return
     */
    public static boolean isMatch(String s, String p) {
        int m = s.length(), n = p.length();
        return dp(s.toCharArray(), 0, p.toCharArray(), 0);
    }

    public static boolean dp(char s[], int i, char p[], int j) {
        int m = s.length, n = p.length;
        //base case
        if (j == n) {
            return i == m;
        }
        if (i == m) {
            //必须成对出现
            if ((n - j) % 2 == 1) {
                return false;
            }
            //判断x*y*z*的情况
            for (; j + 1 < n; j += 2) {
                if (p[j + 1] != '*')
                    return false;
            }
            return true;
        }
        //如果当前字符匹配
        if (s[i] == p[j] || p[j] == '.') {
            //后面一个元素是*
            if (j < n - 1 && p[j + 1] == '*') {
                //*可以匹配一次或者多次
                return dp(s, i, p, j + 2) || dp(s, i + 1, p, j);
            } else {
                //直接下一个比对
                return dp(s, i + 1, p, j + 1);
            }
        } else {
            //当前字符不相等
            if (j < n - 1 && p[j + 1] == '*')
                //如果下一个元素是*，则匹配一次
                return dp(s, i, p, j + 2);
            else {
                //下一个元素不是*直接返回false
                return false;
            }
        }
    }
}
//时间复杂度: O(n^2)
//空间复杂度: O(1)