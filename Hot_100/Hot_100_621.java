package Hot_100;

import java.util.Scanner;

//任务调度器
public class Hot_100_621 {
    /**
     * 主函数
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化数组
        int n = sc.nextInt();
        int k=sc.nextInt();
        //不能用sc.nextLine
        String str = sc.next();
        //调用处理函数并输出结果
        System.out.println(leastInterval(str.toCharArray(),k));
    }

    /**
     * 求最短时间
     *
     * @param tasks
     * @param n
     * @return
     */
    public static int leastInterval(char[] tasks, int n) {
        //统计tasks数组中每个字符的个数
        int[] arr = new int[26];
        for (char c : tasks) {
            arr[c - 'A']++;
        }
        int max = 0;
        //找到出现次数最多的字符
        for (int i = 0; i < 26; i++) {
            max = Math.max(max, arr[i]);
        }
        //计算前n-1行n的间隔的时间大小
        int ret = (max - 1) * (n + 1);
        //添加和出现字符次数最多的字符
        for (int i = 0; i < 26; i++) {
            if (arr[i] == max) {
                ret++;
            }
        }
        //在tasks的长度和ret中取较大的一个
        return Math.max(ret, tasks.length);
    }
}
//时间复杂度: O(n)
//空间复杂度: O(1)