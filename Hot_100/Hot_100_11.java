package Hot_100;

import java.util.Scanner;

//盛最多水的容器
public class Hot_100_11 {
    /**
     * 主函数
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化数组
        int n = sc.nextInt();
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        //调用处理函数并输出结果
        System.out.println(maxArea(arr));
    }

    /**
     * 求面积
     *
     * @param height
     * @return
     */
    public static int maxArea(int[] height) {
        //双指针
        int left = 0, right = height.length - 1;
        int res = 0, area = 0;
        while (left < right) {
            //计算面积
            area = Math.min(height[left], height[right]) * (right - left);
            res = Math.max(res, area);
            //小的一边，向前移动
            if (height[left] < height[right]) {
                left++;
            } else {
                right--;
            }
        }
        return res;
    }
}
//时间复杂度: O(n)
//空间复杂度: O(1)