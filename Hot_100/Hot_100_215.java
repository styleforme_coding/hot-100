package Hot_100;

import java.util.Arrays;
import java.util.Scanner;

//数组中的第k个最大元数
public class Hot_100_215 {
    /**
     * 主函数
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化数组
        int n = sc.nextInt();
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        //输入k
        int k = sc.nextInt();
        //调用处理函数并输出结果
        System.out.println(findKthLargest(arr, k));
    }

    /**
     * 求第k大数
     *
     * @param nums
     * @param k
     * @return
     */
    public static int findKthLargest(int[] nums, int k) {
        Arrays.sort(nums);
        return nums[nums.length - k];
    }
}
//时间复杂度: O(nlog^n)
//空间复杂度: O(1)