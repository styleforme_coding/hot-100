package Hot_100;

import java.util.Scanner;

//买卖股票的最佳时机
public class Hot_100_121 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化数组
        int n = sc.nextInt();
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        //调用处理函数并输出结果
        System.out.println(maxProfit(arr));
    }

    /**
     * 求最大利润
     *
     * @param prices
     * @return
     */
    public static int maxProfit(int[] prices) {
        int min = Integer.MAX_VALUE;
        int ans = 0;
        for (int i = 0; i < prices.length; i++) {
            //找出最低价格
            min = Math.min(min, prices[i]);
            //找出卖出的最大利润
            ans = Math.max(ans, prices[i] - min);
        }
        return ans;
    }
}
//时间复杂度: O(n)
//空间复杂度: O(1)