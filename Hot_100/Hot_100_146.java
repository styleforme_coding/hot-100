package Hot_100;

import java.util.LinkedHashMap;
import java.util.Scanner;

//LRU
public class Hot_100_146 {
    //定义
    static int cap=3;
    static LinkedHashMap<Integer, Integer> cache = new LinkedHashMap<>();
    /**
     * 主函数
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //输入要put几个key
        int n=sc.nextInt();
       while(n-->0){
           //输入put()key value
           int putkey = sc.nextInt();
           int value = sc.nextInt();
           //调用put函数
           put(putkey, value);
       }
        //输入get()key
        int getkey=sc.nextInt();
        //调用get函数
        System.out.println(get(getkey));
    }

    /**
     * get函数
     *
     * @param key
     * @return
     */
    public static int get(int key) {
        if (!cache.containsKey(key)) {
            return -1;
        }
        recently(key);
        return cache.get(key);
    }

    /**
     * put函数
     *
     * @param key
     * @param value
     */
    public static void put(int key, int value) {
        if (cache.containsKey(key)) {
            cache.put(key, value);
            recently(key);
            return;
        }
        if (cache.size() >= cap) {
            int oldestkey = cache.keySet().iterator().next();
            cache.remove(oldestkey);
        }
        cache.put(key, value);
    }

    /**
     * recentle函数
     *
     * @param key
     */
    public static void recently(int key) {
        int val = cache.get(key);
        cache.remove(key);
        cache.put(key, val);
    }
}
//时间复杂度: put() O(1) get() O(1)
//空间复杂度: O(n)