package Hot_100;

import java.util.Scanner;

//编辑距离
public class Hot_100_72 {
    /**
     * 主函数
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        //初始化字符串
        String s1=sc.nextLine();
        String s2=sc.nextLine();
        //调用处理函数并输出结果
        System.out.println(minDistance(s1,s2));
    }

    /**
     * 求最小编辑距离
     * @param word1
     * @param word2
     * @return
     */
    public static int minDistance(String word1, String word2) {
        int m=word1.length(),n=word2.length();
        int dp[][]=new int[m+1][n+1];
        //base case
        for(int i=1;i<=m;i++){
            dp[i][0]=i;
        }
        for(int j=1;j<=n;j++){
            dp[0][j]=j;
        }
        //自底向上求解
        for(int i=1;i<=m;i++){
            for(int j=1;j<=n;j++){
                //相等什么都不做
                if(word1.charAt(i-1)==word2.charAt(j-1)){
                    dp[i][j]=dp[i-1][j-1];
                }else{
                    //选插入、删除、替换中最小的
                    dp[i][j]=Math.min(Math.min(dp[i][j-1]+1,dp[i-1][j]+1),dp[i-1][j-1]+1);
                }
            }
        }
        return dp[m][n];
    }
}
//时间复杂度: O(n^2)
//空间复杂度: O(mn)
