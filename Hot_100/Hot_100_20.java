package Hot_100;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

//有效的括号
public class Hot_100_20 {
    /**
     * 主函数
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //输入字符串
        String str = sc.nextLine();
        //调用处理函数并输出结果
        System.out.println(isValid(str));
    }

    /**
     * 判断有效函数
     *
     * @param s
     * @return
     */
    public static boolean isValid(String s) {
        int n = s.length();
        //有效的括号一定是对称的
        if (n % 2 == 1) {
            return false;
        }
        //用HashMap映射括号
        Map<Character, Character> map = new HashMap<Character, Character>() {{
            put(')', '(');
            put(']', '[');
            put('}', '{');
        }};
        //存左括号
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (map.containsKey(ch)) {
                if (stack.isEmpty() || stack.peek() != map.get(ch)) {
                    return false;
                }
                //左括号和右括号对称，则弹出
                stack.pop();
            } else {
                stack.push(ch);
            }
        }
        return stack.isEmpty();
    }
}
//时间复杂度: O(n)
//空间复杂度: O(n)