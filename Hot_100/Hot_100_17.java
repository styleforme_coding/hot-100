package Hot_100;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

//电话号码的字母组合
public class Hot_100_17 {
    /**
     * 主函数
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化字符串
        String str = sc.nextLine();
        List<String> ans = letterCombinations(str);
        for (String s : ans) {
            System.out.print(s + " ");
        }
    }

    // 每个数字到字母的映射
    static String[] str = new String[]{
            "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"
    };

    static List<String> res = new LinkedList<>();

    public static List<String> letterCombinations(String digits) {
        if (digits.isEmpty()) {
            return res;
        }
        // 从 digits[0] 开始进行回溯
        backtrack(digits, 0, new StringBuilder());
        return res;
    }

    // 回溯算法主函数
    public static void backtrack(String digits, int start, StringBuilder sb) {
        if (sb.length() == digits.length()) {
            // 到达回溯树底部
            res.add(sb.toString());
            return;
        }
        // 回溯算法框架
        for (int i = start; i < digits.length(); i++) {
            int digit = digits.charAt(i) - '0';
            for (char c : str[digit].toCharArray()) {
                // 做选择
                sb.append(c);
                // 递归下一层回溯树
                backtrack(digits, i + 1, sb);
                // 撤销选择
                sb.deleteCharAt(sb.length() - 1);
            }
        }
    }
}
//时间复杂度: O(3^m+4^n)
//空间复杂度: O(n)