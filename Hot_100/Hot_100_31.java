package Hot_100;

import java.util.Scanner;

//下一个排列
public class Hot_100_31 {
    /**
     * 主函数
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化数组
        int n = sc.nextInt();
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        //调用处理函数
        nextPermutation(arr);
        //遍历数组
        for(int i=0;i<n-1;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println(arr[arr.length-1]);
    }

    /**
     * 下一个排列
     * @param nums
     */
    public static void nextPermutation(int[] nums) {
        int i=nums.length-2;
        //找较小数
        while(i>=0&&nums[i]>=nums[i+1]){
            i--;
        }
        if(i>=0){
            //找较大数
            int j=nums.length-1;
            while(j>=0&&nums[j]<=nums[i]){
                j--;
            }
            //交换较小数和较大数
            swap(nums,i,j);
        }
        //翻转i+1-n 降序-升序
        resver(nums,i+1);
    }

    /**
     * 交换函数
     * @param nums
     * @param i
     * @param j
     */
    public static void swap(int nums[],int i,int j){
        int t=nums[i];
        nums[i]=nums[j];
        nums[j]=t;
    }

    /**
     * 翻转函数
     * @param nums
     * @param start
     */
    public static void resver(int nums[],int start){
        int left=start,right=nums.length-1;
        while(left<right){
            swap(nums,left,right);
            left++;
            right--;
        }
    }
}
