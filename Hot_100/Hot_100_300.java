package Hot_100;

import java.util.Arrays;
import java.util.Scanner;

//最长递增子序列
public class Hot_100_300 {
    /**
     * 主函数
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        //初始化数组
        int n=sc.nextInt();
        int arr[]=new int[n];
        for(int i=0;i<n;i++){
            arr[i]=sc.nextInt();
        }
        //调用初始化函数并输出结果
        System.out.println(lengthOfLIS(arr));
    }

    /**
     * 求最长递增子序列
     * @param nums
     * @return
     */
    public static int lengthOfLIS(int[] nums) {
    int n=nums.length;
    int dp[]=new int[n];
    //base case
    Arrays.fill(dp,1);
    for(int i=0;i<n;i++){
        for(int j=0;j<i;j++){
            if(nums[i]>nums[j])
            //转移方程
            dp[i]=Math.max(dp[i],dp[j]+1);
        }
    }
    int ans=Integer.MIN_VALUE;
    //遍历求最大值
    for(int i=0;i<dp.length;i++){
        ans=Math.max(ans,dp[i]);
    }
    return ans;
    }
}
//时间复杂度: O(n)
//空间复杂度: O(n)