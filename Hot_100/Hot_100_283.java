package Hot_100;

import java.util.Scanner;

//移动0
public class Hot_100_283 {
    /**
     * 主函数
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化数组
        int n = sc.nextInt();
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        //调用处理函数
        moveZeroes(arr);
    }

    /**
     * 移除0
     *
     * @param nums
     */
    public static void moveZeroes(int[] nums) {
        int k = 0;
        //把零过滤掉
        for (int num : nums) {
            if (num != 0) {
                nums[k++] = num;
            }
        }
        //不满长度末尾补0
        while (k < nums.length) {
            nums[k++] = 0;
        }
        //遍历数组
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i] + ",");
        }
        System.out.println(nums[nums.length - 1]);
    }
}
//时间复杂度: O(n)
//空间复杂度: O(1)